# Structurez vos documents HTML à l'aide d'éléments sémantiques

HTML est le fondement incontournable de toute page web. Se lancer sur React sans une maîtrise solide d'HTML (ou JS, ou CSS) est une erreur fréquente.

De nombreux développeurs utilisent React depuis des années, mais n'ont pas encore une compréhension claire de certains concepts fondamentaux de l'HTML.

___

## Objectifs de l'article

- [x] Introduire le concept d'HTML sémantique et de structure du document.
- [x] Donner un aperçu de quelques éléments sémantiques, souvent plus appropriés que `<div>` et `<span>` qui sont utilisés de manière excessive.
- [x] Vous guider dans le choix de l'élément le plus pertinent selon le contexte.
- [x] Montrer comment l'utilisation d'éléments sémantiques peut considérablement améliorer la qualité et l'accessibilité de votre application.

___

## Introduction

{==

Écrire de l'HTML sémantique signifie utiliser les éléments HTML les plus appropriés en fonction de leur <u>signification</u>, et non de leur apparence.

==}

??? example "Contre-exemple 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;❌ Une `<div>` maquillée en `<button>` dans un formulaire"

    Un exemple fréquent de mauvaise pratique qui rend votre site inaccessible est d'utiliser `<div>` pour tout.
    A la place d'un `<button>` par exemple, en lui donnant une classe css pour lui donner l'apparence d'un bouton.
    ___
    Une `<div>` n'a aucune signification, c'est un élément qui n'est là <u>que pour donner de l'apparence via CSS</u>.

    Un `<button>`, au contraire, a une signification, un rôle, un état, il est focusable, automatiquement ajouté à la séquence de tabulation du document et donc accessible en naviguation clavier, l'action du clique est déclenchée en pressant les touches ENTER ou ESPACE, etc.

??? example "Contre-exemple 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;❌ Un titre `<h5>` pour mettre en forme du texte"

    A l'inverse de l'exemple précédent, utiliser `<h5>` ou une autre balise de titre juste pour mettre en forme du texte est tout aussi faux.
    ___
    En plus d'ajouter une nouvelle [section](https://developer.mozilla.org/fr/docs/Web/HTML/Element#sectionnement_du_contenu) dans la structure du document, `<h5>` signifie
    que le texte qu'il contient est un titre de niveau 5 dans la page. C'est encore pire si vous utilisez les différents niveaux de titres `<h1>-<h6>` dans le désordre.

    Si le but est de mettre en forme, `<div>` ou `<span>` sont les candidats idéaux car ils ne sont fait que pour ça et n'ont aucune signification particulière.


Lorsque le navigateur parse un document HTML, il construit le Document Object Model (DOM) et le CSS Object Model (CSSOM).

Ensuite, il construit un ==**Accessibility Object Model (AOM)**== utilisé par les lecteurs d'écrans pour interpréter le contenu.


=== "DOM"

    Le DOM est une structure en arbre qui contient tous les éléments du document.

    <figure markdown>
      ![Document Object Model vu depuis les DevTools](DOM.png)
      <figcaption>Vue du Document Object Model (DOM) depuis les DevTools</figcaption>
    </figure>

=== "AOM"

    L'AOM est une version ==sémantique== du DOM. Un sous-ensemble filtré du DOM, qui n'inclut que les éléments qui ont une signification (= sémantique).

    <figure markdown>
      ![Accessibility Object Model vu depuis les DevTools](AOM.png)
      <figcaption>Vue de l'Accessibility Object Model (AOM) [depuis les DevTools](https://developer.chrome.com/docs/devtools/accessibility/reference/#explore-tree)</figcaption>
    </figure>

___

## Qu'est-ce qu'un élément sémantique ?

Les éléments sémantiques offrent une ==signification contextuelle== à votre contenu.

Par exemple, la balise `<article>` indique que le contenu est une section distincte et autonome
qui peut être sorti de votre application pour être réutilisé de manière indépendante.

Cette sémantique permet aux navigateurs, aux lecteurs d'écran et aux moteurs de recherche de mieux comprendre la structure de la page,
améliorant ainsi l'accessibilité et le référencement.

___

## Inconvénients des `<div>` et `<span>`

Les balises `<div>` et `<span>` n'ont aucune valeur sémantique; elles n'ont aucune signification contextuelle.

Si vous n'utilisez que des `<div>` et des `<span>` partout, les lecteurs d'écrans et moteurs de recherche peuvent avoir du mal à discerner la hiérarchie du contenu.
Cela peut entraver l'accessibilité, car les lecteurs d'écran dépendent de la structure sémantique (AOM) pour communiquer efficacement le contenu aux utilisateurs.

Ces éléments restent toutefois très utiles et **vous devez continuer de les utiliser**, mais toujours en vous posant l'une de ces deux questions:

1. 💡 ==Cette `<div>` sert-elle uniquement à la mise en forme ?==

     - [x] **Oui** - `<div>` est l'élément le plus approprié.
     - [ ] **Non** - Utiliser un élement sémantique<br><br>

2. 💡 ==Cette zone doit-elle apparaître sur la structure sémantique du document (AOM) ?==

     - [x] **Oui** - Utiliser un élément sémantique
     - [ ] **Non** - Utiliser une `<div>`.

!!! note "Note sur `<span>`"

    `<span>` ne devrait être utilisé que pour mettre en forme du contenu phrasé.

    N'utilisez pas `<span>` en lui appliquant la propriété `display: block`.

___

## Structurer le contenu avec des éléments sémantiques

En dehors de `<div>` et `<span>`, presque tous les éléments HTML ont une sémantique.

Ceux que je présente ici permettent de structurer le contenu de manière logique.

<aside markdown>
<small>Certains de ces éléments (`<h1>`-`<h6>`, `<section>`, `<article>`, `<footer>`) introduisent une nouvelle [section](https://developer.mozilla.org/fr/docs/Web/HTML/Element#sectionnement_du_contenu) dans le plan du document et d'autres sont purement informatifs (`<main>`, `<header>`, ...).</small>
</aside>

=== "**`<article>`**"

    👉 [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/article)

    Délimite un contenu autonome qui doit pouvoir être sorti de votre application pour être réutilisé de manière indépendante.

    !!! info "Règles"

        - Un `<article>` autonome, qui n'est pas imbriqué dans un autre élément `<article>`, devrait être identifié par un titre `<h1>` à `<h6>`.
        - Un `<article>` imbriqué dans un autre représente un article relatif à son parent. Par ex. les commentaires d'un article de blog.
          Chaque commentaire est un `<article>` inclus dans l'article qui lui-même est un `<article>`.


=== "**`<section>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/section)

    Organise le document en sections distinctes, par exemple un groupe de contenu thématique.

    Une section sert généralement à lier ensemble un titre `<h1>`-`<h6>` à son contenu.

    Lorsque vous hésitez entre `<section>` et `<article>`, imaginez votre page comme un journal composé de sections et d'articles qui, à leur tour, peuvent
    également contenir des sections contenant d'autres articles imbriqués.

    !!! info "Règles"

        - Chaque `<section>` devrait être identifiée grâce à un titre `<h1>`-`<h6>`.
        - Si le sectionnement du contenu sert uniquement à la mise en forme et ne doit pas apparaître sur le plan du document, utiliser `<div>`.

    !!! tip

        Si le contenu peut être séparé du reste de la page sans perdre son contexte, utiliser `<article>`.

=== "**`<header>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/header)

    Représente un contenu introductif comme l'en-tête principale de votre application, l'en-tête d'une section ou d'un article.

=== "**`<nav>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/nav)

    Section destinée à la navigation. Vous pouvez en avoir plusieurs par document.

=== "**`<footer>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/footer)

    Représente le pied de page de la section parente.

    Contient généralement des informations sur l'auteur, le copyright, ou des liens.

    !!! info "Règles"

        - Les informations sur l'auteur doivent être placées dans un élément `<address>`.
        - Si le sectionnement du contenu sert uniquement à la mise en forme et ne doit pas apparaître sur le plan du document, utiliser `<div>`.


=== "**`<aside>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/aside)

    Représente une aparté. Une partie du document dont le contenu n'a qu'un rapport indirect avec le contenu principal.

=== "**`<main>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/main)

    Représente le contenu principal du document ou la fonctionnalité principale d'une application.

    !!! info "Règles"

        - Un document ne peut pas avoir plus d'un seul élément `<main>` sans attribut `hidden`.

=== "**`<address>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/address)

    Sert à indiquer des informations de contact telles que nom, prénom, email, téléphone, adresse physique, url, coordonnées géographiques, etc.

=== "**`<time>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/time)

    Représente une heure, une date, ou une durée dans un format informatique qui permet aux moteurs de recherche d'exploiter ces données. 

=== "**`<h1>`-`<h6>`**"

    👉️ [mdn](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Heading_Elements)

    Représentent les 6 niveaux de titre de section.

    Les titres peuvent être utilisés par les navigateurs ou lecteurs d'écran pour construire automatiquement la table des matières d'un document.

___

## Sémantique du texte en ligne

Même les éléments tels que `<b>`, `<strong>`, `<i>`, `<em>`, `<u>`, `<small>`, `<s>`, `<sub>`, `<sup>`, ... [ont une sémantique](https://developer.mozilla.org/fr/docs/Web/HTML/Element#s%C3%A9mantique_du_texte_en_ligne).

Ils ne doivent <u>pas être utilisés uniquement</u> pour mettre en forme du texte.

`<strong>` par exemple, met le texte en gras, mais indique que le texte est d'une importance particulière. Si on ne cherche qu'à mettre en gras, il faut utiliser `<span>` avec la propriété CSS `font-weight`.


---

## ARIA role

!!! info ""

    Je ne parlerai pas d'[ARIA](https://w3c.github.io/aria/#terms) (Accessible Rich Internet Applications) dans cet article.
    Un autre article sera dédié à l'accessibilité (souvent abbrégé [a11y](https://www.a11yproject.com) pour les curieux 😉).

L'attribut `role` décrit le rôle qu'a un élément dans le contexte du document. En d'autres termes, il lui donne une sémantique.
C'est un attribut global et donc valide sur tous les éléments HTML.

Les éléments sémantiques ont tous un `role` implicite par défaut, certain dépendant du contexte.
Par exemple, `<header>` n'a pas le même `role` lorsque son parent est `<body>` que lorsqu'il est imbriqué dans un `<article>`.

Vous trouvez ici toutes les informations nécessaires sur les [roles ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles).

{==

La sémantique d'un élément est en fait déterminée par ce `role` ARIA, qui est utilisé dans l'AOM et
donc important pour les lecteurs d'écrans et pour certains moteurs de recherche.

==}

Les éléments interactifs tels que les boutons, les liens, les checkboxes, etc ont tous un `role` défini.

Ainsi, si une div joue le rôle de bouton mais n'a pas le `role="button"`, la div ne sera pas accessible dans l'AOM.
En donnant à votre `div` le `role="button"` vous la transformez sémantiquement en un bouton.
Par contre, vous ne pourrez toujours pas y accéder via la navigation au clavier.
Il est donc préférable d'utiliser l'élément `<button>` qui a le bon role et sera automatiquement ajouté à la séquence de tabulation du document.

___

## Exemple

L'usage excessif de balises `<div>` ou `<span>`, ==qui n'ont aucune valeur sémantique==, peut nuire à l'accessibilité et à la compréhension du contenu
par les moteurs de recherche.

Prenez n'importe quel projet web dans l'entreprise (React, Play ou autre) et ouvrez un composant au hasard.
Il y a de fortes chances que vous tombiez sur un exemple de tout ce qu'il ne faut pas faire 🤪.

En voici un que j'ai pioché (je vous promets que je n'ai pas eu à fouiller 🤭) :

=== "❌ Version originale (Mauvais code HTML)"

    ```html linenums="1"
    <div className={`cp-tools`}>
      <div className={`cp-tools-tgl`} onClick={this.onClickTools}>
        <span className="ic-open"><Icon iconName="Add" size={30} /></span>
        <span className="ic-close"><Icon iconName="Interrogation" size={30} /></span>
        <span className="tgl-label">Fermer</span>
      </div>
    
      <div className="cp-tools-panels">
        <div className="cp-accordeon">
            <React.Fragment>
                <div className="cp-tools-panels-item">
                  <div className="" style={{ paddingTop: 13 }}>
                    <span className="cp-tools-panels-titre">Besoin d'aide ?</span> <br />
                    <span className="cp-tools-panels-aide">Contactez le service des offres</span><br />
                    <div className="ic-phone">
                      <Icon iconName="Phone" size={20} />
                    </div>
                    <div className="contact">0800 800 800</div>
                    <div className="horaires">de 08:00 à 19:00</div>
                    <div className="horaires">du lundi au vendredi</div>
                    <div className="ic-mail">
                      <Icon iconName="Email" size={20} />
                    </div>
                    <div className=" contact"><span className='email'>offres@email.com</span></div>      
                  </div>
                </div>
            </React.Fragment>
        </div>
      </div>
    </div>
    ```

=== "✔️ Version améliorée"

    ```html linenums="1"
    <details className="cp-tools">
      <summary onClick={handleClickSummary}>
        {!toolsOpened && <div className="ic-open"><Icon iconName="Add" size={30} /></div>}
        {toolsOpened && <div className="ic-close"><Icon iconName="Interrogation" size={30} /></div>}
        <div className="label">{toolsOpened ? "Fermer" : "Besoin d'aide ?"}</div>
      </summary>
      
      <article id="tools-pane" className="panels">
          <div className="item">
            <header>
              <h2>Besoin d'aide ?</h2>
              <div>Contactez le service des offres</div>
            </header>
            <section>
              <header>
                <Icon iconName="Phone" size={20} />
              </header>
              <address><a href="tel:0800 808 848">0800 808 848</a></address>
              <p>De <time datetime="08:00">08:00</time> à <time datetime="19:00">19:00</time><br />
                du lundi au vendredi</p>
            </section>
            <section>
              <header>
                <Icon iconName="Email" size={20} />
              </header>
              <address><a href="mailto:offres@email.com">offres@email.com</a></address> 
            </section>     
          </div>
      </article>
    </details>
    ```

!!! abstract ""

    Les choix qui sont fait dans la **version améliorée** sont subjectifs. Il n'y a jamais qu'une seule bonne façon de faire.


Vous remarquerez que beaucoup de `className` ont pu être retirés dans la version améliorée, ce qui allège aussi la structure.
L'utilisation d'éléments sémantiques différents permet d'appliquer les styles css directement sur ces éléments.

Voici l'AOM correspondant à cet exemple:

=== "❌ Version originale (Mauvais code HTML)"

     <figure markdown>
      <img alt="AOM du code mal structuré et sans sémantique" src="../AOM-awful-code.png" style="margin: auto" />
      <figcaption>Rien n'est structuré, les éléments n'ont aucun role et aucune signification dans le contexte du document.</figcaption>
    </figure>

=== "✔️ Version améliorée"

    <figure markdown>
      <img alt="AOM du code propre, panneau fermé" src="../AOM-clean-code-expanded-false.png" style="margin: auto" />
      <figcaption>Lorsque le panneau est fermé, son contenu n'est pas inclus dans l'arbre d'accessibilité</figcaption>
    </figure>

    <figure markdown>
      <img alt="AOM du code propre, panneau ouvert" src="../AOM-clean-code-expanded-true.png" style="margin: auto" />
      <figcaption>Les éléments sémantiques ont un role associé qui leur donne un sens dans le contexte du document.</figcaption>
    </figure>


Cet exemple montre que l'utilisation d'éléments sémantiques appropriés éclaircit la structure du code.

- [x] En Java, Typescript ou autres languages de programmation, un code "clean" doit être bien structuré et lisible simplement.

- [x] En HTML, c'est le même principe. L'utilisation d'éléments sémantiques et structurants permet d'obtenir un code "clean" beaucoup plus lisible, autant pour les humains que la machine.


___


## Best practices et Conseils

- Lorsque vous choisissez des éléments sémantiques, tenez compte du contexte et de la signification du contenu.
- Ne surchargez pas inutilement votre structure avec des éléments sémantiques, mais utilisez-les judicieusement pour améliorer la lisibilité.

**`<h1>`-`<h6>`**

- N'utilisez pas les titres `<h1>`-`<h6>` pour ajuster la taille du texte. Pour cela, utilisez la propriété CSS `font-size`.
- Ne sautez pas de niveaux entre les titres: on commence par `<h1>`, puis `<h2>` et ainsi de suite.
- Evitez d'avoir plusieurs éléments `<h1>` sur une page.

**`<span>`**

- Ne doit être utilisé que pour ==mettre en forme **du texte** avec css== lorsqu'aucun autre élément sémantique n'est approprié. Ne doit pas remplacer `<div>` avec `display: block`.

**`<div>`**

- A utiliser lorsque l'élément ne sert qu'à la mise en forme et ne doit pas apparaître sur le plan du document.

**`<button>`**

- N'utilisez rien d'autre que `<button>` pour représenter un bouton. Meme si vous utilisez `<a role="button">`, vous devez vous assurer d'implémenter en js le clique via la barre d'espace comme c'est le cas pour `<button>`.

**`<b>`, `<strong>`**

- Préférez `<span style="font-weight: bold">` si vous ne souhaitez que mettre en forme sans donner une signification plus importante au texte.


___

## Conclusion

Les éléments sémantiques améliorent l'accessibilité en facilitant la navigation des lecteurs d'écran.

Ils contribuent aussi à une expérience utilisateur améliorée en rendant la structure du contenu plus claire et intuitive.

Une utilisation appropriée des éléments sémantiques aboutit à une application web plus cohérente et professionnelle.
En résumé, vous améliorez sa structure, son accessibilité et sa compréhension par les moteurs de recherche.

<br />
___

## Sources


- [📚 MDN - Sectionnement du contenu](https://developer.mozilla.org/fr/docs/Web/HTML/Element#sectionnement_du_contenu)
- [📚 WHATWG - HTML Specification](https://html.spec.whatwg.org/dev/dom.html#dom)
- [📚 MDN - ARIA Roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles)
- [📚 The a11y Project](https://www.a11yproject.com)
- [📚 W3C ARIA specification](https://w3c.github.io/aria/#terms)
- [📚 Chrome Developers - Explore the full-page accessibility tree](https://developer.chrome.com/docs/devtools/accessibility/reference/#explore-tree)


<br />